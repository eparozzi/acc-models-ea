// H2 beam line in MAD-X
// N. Charitonidis, EN/EA-LE,  October 2019
// Version for the EN/EA-LE database - FM optics 
// Suitable for simple optics studies - **not to be used for precision calculations**
// Based on the format of H8 - Alexander based. 

title, H2BEAMLINE;


QSL : QUADRUPOLE, l = 3.00000, APERTYPE=CIRCLE, APERTURE=0.050 ; 
QNL : QUADRUPOLE, l = 2.99000, APERTYPE=CIRCLE, APERTURE=0.040 ; 
QWL : QUADRUPOLE, l = 2.94800, APERTYPE=CIRCLE, APERTURE=0.050 ;


MTN : RBEND, l = 3.60000, APERTYPE=RECTANGLE, APERTURE={0.200,0.026} ;
MSN : RBEND, l = 3.60000, APERTYPE=RECTANGLE, APERTURE={0.114, 0.060};
MBN : RBEND, l = 5.00000, APERTYPE=RECTANGLE, APERTURE={0.0645, 0.03} ; 

B003 : MTN, angle=0.0056 ; 						
B007 : MTN, angle=0.0056 ; 						
B027 : MSN, angle=0.0280 ; 						
// B031 : MSN, angle=0. ;     					
Q038 : QNL, k1= 0.015317631351171 ;  			
Q041 : QSL, k1=0.005979344860000 ;				
Q049 : QNL, k1=-0.016747098250836 ;				
B053 : MBN, angle=0.0068356, tilt=1.5708;		
B059 : MBN, angle=0.0068356, tilt=1.5708;       
B065 : MBN, angle=0.0068356, tilt=1.5708;       
B071 : MBN, angle=0.0068356, tilt=1.5708; 		
B077 : MBN, angle=0.0068356, tilt=1.5708;       
B083 : MBN, angle=0.0068356, tilt=1.5708;       
Q087 : QNL, k1=-0.015230100692308 ;				
Q099 : QNL, k1=0.020121624060201 ;				
Q134 : QWL, k1=-0.013552011963211;				
Q170 : QNL, k1=0.020121624060201;				
Q182 : QNL, k1=-0.015230100692308;				
Q220 : QNL, k1=-0.015230100692308;				
Q231 : QNL, k1=0.020121624060201;				
Q267 : QWL, k1=-0.013552011963211;				
Q303:  QNL, k1=0.020104879936455;				
Q314 : QNL, k1=-0.015208744414716;				
B319 : MBN, angle=-0.0068482, tilt=1.5708;		
B325 : MBN, angle=-0.0068482, tilt=1.5708;		
B330 : MBN, angle=-0.0068482, tilt=1.5708;      
B337 : MBN, angle=-0.0068482, tilt=1.5708;		
B343 : MBN, angle=-0.0068482, tilt=1.5708;		
B348 : MBN, angle=-0.0068482, tilt=1.5708;		
Q353 : QNL, k1=-0.015208744414716 ;				
Q364 : QNL, k1= 0.020104879936455 ;				
Q400 : QNL, k1= -0.007447692662207 ;			
B405 : MBN, angle=0.006;						
Q447 : QNL, k1=0.018222419317726;				
Q457 : QWL, k1=-0.015090700013378;				
Q470 : QWL, k1=0.002520408391304;				
Q479 : QNL, k1=-0.0;							
Q489 : QNL, k1=-0.006930463003344;				
Q500 : QNL, k1=0.0;								
Q511 : QNL, k1=-0.003657270892977;				
Q524 : QNL, k1=0.010399471123746;				


H2BEAMLINE: SEQUENCE, refer = centre, l = 600.0 ;
B003 : B003, at = 4.950 - 3.60/2 ;
B007 : B007, at = 9.150 - 3.60/2 ;
B027 : B027, at = 28.850 - 3.20/2 ;
// B031 : B031, at = 32.400 - 3.20/2 ;
Q038 : Q038, at = 39.215 - 2.99/2 ;
Q041 : Q041, at = 42.950 - 3.00/2 ;
Q049 : Q049, at = 50.215 - 2.99/2 ;
B053 : B053, at = 55.765 - 5.00/2 ;
B059 : B059, at = 61.425 - 5.00/2 ;
B065 : B065, at = 67.085 - 5.00/2 ;
B071 : B071, at = 73.895 - 5.00/2 ;
B077 : B077, at = 79.555 - 5.00/2 ; 
B083 : B083, at = 85.215 - 5.00/2 ; 
Q087 : Q087, at = 88.755 - 2.99/2 ;
Q099 : Q099, at = 100.103 - 2.99/2 ;
Q134 : Q134, at = 135.929 - 2.948/2 ;
Q170 : Q170, at = 171.737 - 2.99/2 ;
Q182 : Q182, at = 183.085 - 2.99/2 ;
Q220 : Q220, at = 221.625 - 2.99/2 ;
Q231 : Q231, at = 232.973 - 2.99/2 ;
Q267 : Q267, at = 268.799 - 2.948/2 ;
Q303:  Q303, at = 304.607 - 2.99/2 ;
Q314 : Q314, at = 315.955 - 2.99/2 ;
B319 : B319, at = 321.505 - 5.00/2 ;
B325 : B325, at = 327.165 - 5.00/2 ;
B330 : B330, at = 332.825 - 5.00/2 ;
B337 : B337, at = 339.635 - 5.00/2 ;
B343 : B343, at = 345.295 - 5.00/2 ;
B348 : B348, at = 350.955 - 5.00/2 ;
Q353 : Q353, at = 354.495 - 2.99/2 ;
Q364 : Q364, at = 365.843 - 2.99/2 ;
Q400 : Q400, at = 401.660 - 2.99/2 ;
B405 : B405, at = 407.980 - 5.00/2 ;
Q447 : Q447, at = 448.825 - 2.99/2 ;
Q457 : Q457, at = 458.553 - 2.948/2 ;
Q470 : Q470, at = 471.021 - 2.948/2 ;
Q479 : Q479, at = 480.791 - 2.99/2 ;
Q489 : Q489, at = 490.351 - 2.99/2 ;
Q500 : Q500, at = 503.151 - 2.99/2 ;
Q511 : Q511, at = 513.035 - 2.99/2 ;
Q524 : Q524, at = 522.941 - 2.99/2 ;



ENDSEQUENCE ;


beam, particle=proton, sequence=H2BEAMLINE, PC=300.0;
use, sequence=H2BEAMLINE;

SELECT,flag=Twiss,clear;
SELECT,flag=Twiss,column=name,s,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26,RE36,RE46,K1L ;
Twiss,chrom=true, rmatrix=true,save,file="h2-fm-twiss.out",
x=0, y=0, px=0.00, py=0, betx=1, alfx=0, bety=1, alfy=0;